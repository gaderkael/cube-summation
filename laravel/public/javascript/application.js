var URL = "/Cubic";
var N;
var M;
var M_actual;
var T;
var T_actual;
var updateForm;
var iniciado = false;
var enable_NM = true;

function initProcess() {
    if (!iniciado) {
        T = T_actual = $("#test-cases-total").html();
    }

    operacionesForm = createDialogForm('operacionesForm', 300, 350, 'Continuar', operacionFunctionForm(), 'Cancelar', cleanCloseOperacion(), cleanCloseOperacion());

    updateForm = createDialogForm('updateForm', 300, 350, 'Continuar', updateFunctionForm(), 'Cancelar', cleanCloseUpdate(), cleanCloseUpdate());

    queryForm = createDialogForm("queryForm", 300, 350, 'Continuar', queryFunctionForm(), 'Cancelar', cleanCloseQuery(), cleanCloseQuery());

}
function createDialogForm(id, height, width, nameOkButton, okButtonFunction, nameCancelButton, cancelButtonFunction, closeFunction) {
    return $("#" + id).dialog({
        autoOpen: false,
        height: height,
        width: width,
        modal: true,
        buttons:
                [
                    {
                        text: nameOkButton,
                        click: okButtonFunction
                    },
                    {
                        text: nameCancelButton,
                        click: cancelButtonFunction
                    }
                ],
        close: function () {
            closeFunction();
        }
    });
}

function operacionFunctionForm(dialogo) {
    // Obtenemos los valores de X, Y y Z
    if (ValidarDatoNumericoInput("#N_Input", "orden de la matriz", 1, 100)) {
        if (ValidarDatoNumericoInput("#M_Input", "la Coordenada Y", 1, 1000)) {
            setNM($("#N_Input").val(), $("#M_Input").val());
            dialogo.dialog("close");
            cleanCloseOperacion();

        }
    }
}

function cleanCloseOperacion() {
    $("#N_Input").val('');
    $("#M_Input").val('');
    operacionesForm.dialog("close");
}

function updateFunctionForm() {
    // Obtenemos los valores de X, Y y Z
    if (ValidarDatoNumericoInput("#XU", "la Coordenada X", 1, N)) {
        if (ValidarDatoNumericoInput("#YU", "la Coordenada Y", 1, N)) {
            if (ValidarDatoNumericoInput("#ZU", "la Coordenada Z", 1, N)) {
                if (ValidarDatoNumericoInput("#WU", "'valor a actualizar'", -1 * Math.pow(10, 9), Math.pow(10, 9))) {
                    updateFunction($("#XU").val(), $("#YU").val(), $("#ZU").val(), $("#WU").val());
                    updateForm.dialog("close");
                    cleanCloseUpdate();
                }
            }
        }
    }
}

function cleanCloseUpdate() {
    $("#XU").val('');
    $("#YU").val('');
    $("#ZU").val('');
    $("#WU").val('');
}

function queryFunctionForm() {
    // Obtenemos los valores de X1, Y1, Z1, x2, y2, y z2
    if (ValidarDatoNumericoInput("#X1Q", "la Coordenada X1", 1, N)) {
        if (ValidarDatoNumericoInput("#Y1Q", "la Coordenada Y1", 1, N)) {
            if (ValidarDatoNumericoInput("#Z1Q", "la Coordenada Z1", 1, N)) {
                if (ValidarDatoNumericoInput("#X2Q", "la Coordenada X2", $("#X1Q").val(), N)) {
                    if (ValidarDatoNumericoInput("#Y2Q", "la Coordenada Y2", $("#Y1Q").val(), N)) {
                        if (ValidarDatoNumericoInput("#Z2Q", "la Coordenada Z2", $("#Z1Q").val(), N)) {
                            queryFunction($("#X1Q").val(), $("#Y1Q").val(), $("#Z1Q").val(), $("#X2Q").val(), $("#Y2Q").val(), $("#Z2Q").val());
                            queryForm.dialog("close");
                            $("#X1Q").val('');
                            $("#Y1Q").val('');
                            $("#Z1Q").val('');
                            $("#X2Q").val('');
                            $("#Y2Q").val('');
                            $("#Z2Q").val('');
                        }
                    }
                }
            }
        }
    }
}

function cleanCloseQuery() {
    $("#X1Q").val('');
    $("#Y1Q").val('');
    $("#Z1Q").val('');
    $("#X2Q").val('');
    $("#Y2Q").val('');
    $("#Z2Q").val('');
}

function updateBox() {
    if (!enable_NM) {
        if (existTestCases()) {
            updateForm.dialog('open');
        }
    }
}

function queryBox() {
    if (!enable_NM) {
        if (existTestCases()) {
            queryForm.dialog('open');
        }
    }
}

function enableOrderAndOperation() {
    if (enable_NM) {
        operacionesForm.dialog('open');
    }
}

function setNM(n, m) {
    $.ajax({url: URL + "/setn/" + n, success: function (result) {
            $("#actual_N").html(n);
            $("#actual_M").html(m);
            $("#M_registrado").html(m);
            $("#actual_N_M").show();
            enable_NM = false;
            T_actual--;
            M = M_actual = m;
            N = n;
            insertProgress('Test Case # ' + (T - T_actual), "");
            $("#operationsBox").removeClass('activeButton');
            $("#operationsBox").addClass('inactiveButton');
            $("#updateBox").removeClass('inactiveButton');
            $("#updateBox").addClass('activeButton');
            $("#queryBox").removeClass('inactiveButton');
            $("#queryBox").addClass('activeButton');
            insertProgress('order_oper ' + n + ' ' + m + ' ', '');

        }});


}

function updateFunction(x, y, z, w) {
    $.ajax({url: URL + "/update/" + x + "/" + y + "/" + z + "/" + w, success: function (result) {
            insertProgress('update ' + x + ' ' + y + ' ' + z + ' ' + w + ' ', result);
            updateTestCases();
        }});
}

function queryFunction(x1, y1, z1, x2, y2, z2) {
    $.ajax({url: URL + "/query/" + x1 + "/" + y1 + "/" + z1 + "/" + x2 + "/" + y2 + "/" + z2, success: function (result) {
            insertProgress('queery' + x1 + ' ' + y1 + ' ' + z1 + ' ' + x2 + ' ' + y2 + ' ' + z2, result);
            updateTestCases();
        }});
}

function ValidarDatoNumericoInput(Id, Co, min, max) {
    Coordenada = $(Id).val();
    if (!Coordenada) {
        alert("Debe ingresar " + Co);
        return false;
    }
    if ($.isNumeric(Coordenada)) {
        if (isInt(Coordenada)) {
            if ((min <= Coordenada && Coordenada <= max))
            {
                return true;
            }
        }
    }
    alert("El rango de " + Co + " debe encontrarse en el rango de " + min + " y " + max);
    return false;
}

function existTestCases() {
    if (T_actual >= 0 && M_actual > 0) {
        return true;
    }
    alert('Ya no hay mas Casos de Prueba ni Operaciones');
    return false;
}

function updateTestCases() {
    if (M_actual > 0) {
        M_actual--;
    }
    $("#actual_M").html(M - M_actual + "");
    if (M_actual === 0) {
        console.log('T_actual' + T_actual);
        if (T_actual > 0) {
            $("#actual_N_M").hide();
            M_actual = 0;
            enable_NM = true;
            $("#operationsBox").removeClass('inactiveButton');
            $("#operationsBox").addClass('activeButton');
            $("#updateBox").removeClass('activeButton');
            $("#updateBox").addClass('inactiveButton');
            $("#queryBox").removeClass('    activeButton');
            $("#queryBox").addClass('inactiveButton');
        } else {
            insertProgress('end Test Cases', '');
            $("#actual_N_M").hide();
            $("#updateBox").removeClass('activeButton');
            $("#updateBox").addClass('inactiveButton');
            $("#queryBox").removeClass('activeButton');
            $("#queryBox").addClass('inactiveButton');
        }
    }
}

function insertProgress(function_progress, result_progress) {
    $("#functionsProcess").append(function_progress);
    $("#functionsProcess").append('<br/><br/>');

    $("#resultBox").append(result_progress);
    $("#resultBox").append('<br/><br/>');
}