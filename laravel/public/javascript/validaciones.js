function validaCampos(){
    return validaT()();
}

function validaT() {
    T = $("#test-cases").val();
    if(!T){
        alert("Debe ingresar el numero de pruebas a realizar");
        return false;
    }
    if ($.isNumeric(T)) {
        if (isInt(T)) {
            if (1 <= T && T <= 50)
            {
//                $("#form1").attr("action");
//                $("#form1").attr("action", $("#form1").attr("action")+ "/" + T);
                return true;
            } else {
                alert("El valor ingresado para el numero de casos debe localizarce entre 1 y 50");
                return false;
            }
        }
    }
    alert("Solo se admite valores numericos enteros");
    return false;
}

function validaNM() {
    NM = $("#n-m").val();
    if(!NM){
        alert("Debe ingresar el orden de la matriz y el numero de operaciones");
        return false;
    }
    expresion = /^([0-9]+)(\s)([0-9]+)$/;
    if (!expresion.test(NM)) {
        alert("El valor introdución no cumple con la regla N M (Valor + espacio en blanco + valor)");
        return false;
    }
    data = NM.split(" ");
    // Validación para N
    if(!(1 <= data[0] <= 100)) {
        alert("El valor ingresado para N debe localizarce entre 1 y 100");
        return false;
    }
    // Validación para M
    if(!(1 <= data[1] <= 1000)) {
        alert("El valor ingresado para N debe localizarce entre 1 y 1000");
        return false;
    }
    $("#n").val(data[0]);
    $("#m").val(data[1]);
    return true;
}

function isInt(n) {
    return n % 1 === 0;
}