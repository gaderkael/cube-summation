@extends('layouts.master')

@section('title', 'Paso 1')

@section('menu')
<li><a href="/Cubic"  class="active"><span>Paso 1</span></a></li>
@stop

@section('content')
<form action="/Cubic/process" onsubmit="return validaCampos();" name="form1" id="form1" method="POST">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <p>
        <label for="test-cases">Escribe el Numero de Casos (T)</label>
        <input type="text" name="t" id="test-cases"/>
    </p>
     <p>   
    <button type="submit" form="form1" value="Submit">Continuar</button>
    </p>
</form>
@stop