@extends('layouts.master')

@section('title', 'Paso 1')

@section('onready', 'onload="initProcess()"')

@section('menu')
<li><a href="/Cubic" ><span>Paso 1</span></a></li>
<li><a href="#"  class="active"><span>Proceso</span></a></li>
<div style="display: none;">
    <div id="N_value"></div>
    <div id="T_value"></div>
</div>
@stop

@section('content')
<div>
    Numero de Test Cases asignados <span id="test-cases-total"><?php echo $t ?></span>
</div>
<div class="operationsBox functionBox activeButton" id="operationsBox" onclick="enableOrderAndOperation();">
    Orden y Número de operaciones
    <div id="actual_N_M" style="display: none; text-align: left;">
        <div>
            Orden de la matriz: <span id="actual_N"></span>
        </div>
        <div>
            Numero de operaciones: <span id="M_registrado"></span>
        </div>
        <div>
            Operaciones restantes: <span id="actual_M"></span>
        </div>
    </div>
</div>
<div class="updateBox functionBox inactiveButton" id="updateBox" onclick="updateBox();">
    Update
</div>
<div class="queryBox functionBox inactiveButton" id="queryBox" onclick="queryBox();">
    Query
</div>

<div style="clear: both;"></div>
<div class="Test-Cases">
    <div id="functionsProcess" class="contentBoxTestCases">
        
    </div>
    <div id="resultBox" class="contentBoxTestCases">
        
    </div>
    <div style="clear: both;"></div>
</div>
<div id="operacionesForm" title="Orden de la matriz y número de operaciones" style="display: none;">
    <p class="validateTips">Todos los campos son requiridos</p>

    <input type="text" name="N_Input" id="N_Input" placeholder="Orden de la matriz" class="text ui-widget-content ui-corner-all">
    <br/>
    <input type="text" name="M_Input" id="M_Input" placeholder="Número de operaciones" class="text ui-widget-content ui-corner-all">

    <!-- Allow form submission with keyboard without duplicating the dialog button -->
    <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
</div>
<div id="updateForm" title="Actualizar Valor" style="display: none;">
    <p class="validateTips">Todos los campos son requiridos</p>

    <input type="text" name="XU" id="XU" placeholder="Coordenada X" class="text ui-widget-content ui-corner-all">
    <br/>
    <input type="text" name="YU" id="YU" placeholder="Coordenada Y" class="text ui-widget-content ui-corner-all">
    <br/>
    <input type="text" name="ZU" id="ZU" placeholder="Coordenada Z" class="text ui-widget-content ui-corner-all">
    <br/>
    <input type="text" name="WU" id="WU" placeholder="Valor a introducir" class="text ui-widget-content ui-corner-all">

    <!-- Allow form submission with keyboard without duplicating the dialog button -->
    <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
</div>
<div id="queryForm" title="Ejecutar un query" style="display: none;">
    <p class="validateTips">Todos los campos son requiridos</p>
    <input type="text" name="X1Q" id="X1Q" placeholder="Coordenada X1" class="text ui-widget-content ui-corner-all">
    <input type="text" name="Y1Q" id="Y1Q" placeholder="Coordenada Y1" class="text ui-widget-content ui-corner-all">
    <input type="text" name="Z1Q" id="Z1Q" placeholder="Coordenada Z1" class="text ui-widget-content ui-corner-all">
    <br/>
    <input type="text" name="X2Q" id="X2Q" placeholder="Coordenada X2" class="text ui-widget-content ui-corner-all">
    <input type="text" name="Y2Q" id="Y2Q" placeholder="Coordenada Y2" class="text ui-widget-content ui-corner-all">
    <input type="text" name="Z2Q" id="Z2Q" placeholder="Coordenada Z2" class="text ui-widget-content ui-corner-all">
    <br/>

    <!-- Allow form submission with keyboard without duplicating the dialog button -->
    <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
</div>
@stop