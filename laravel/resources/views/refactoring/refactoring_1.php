<?php

define("TYPE_IPHONE", '1');

define("ERROR_SIN_ERROR", '0');
define("ERROR_SIN_DRIVER", '1');
define("ERROR_ESTATUS_FALLIDO", '2');
define("ERROR_SIN_SERVICIO", '3');
define("ERROR_EN_DATOS_INPUT", '4');

define("STATUS_SIN_DRIVER", '1');
define("STATUS_CON_DRIVER", '2');
define("STATUS_COMPLETADO", '6');

define("DEFAULT__AVAILABLE", '0');

define("PUSH_MESSAGE", 'Tu servicio ha sido confirmado!');

class refactoring {

    public function post_confirm() {
        // Se obtienen los datos del servicio y el driver
        $service_id = $this->get_data('servicio_id');
        $driver_id = $this->get_data('driver_id');

        if (!$service_id || !$driver_id) {
            return $this->response_error(ERROR_EN_DATOS_INPUT);
        }

        $servicio = Service::find($service_id);

        // Se verifica que el Servicio es válido
        $validacion_error = $this->valida_servicio($servicio);
        if ($validacion_error !== TRUE) {
            return $this->response_error($validacion_error);
        }

        // Actualización del estatus 
        $servicio = $this->actualiza_post_confirm($service_id, $driver_id);
        
        //Notificar a usuario !!
        return $this->enviar_notificacion($servicio, PUSH_MESSAGE);
    }

    /**
     * Obtiene el valor del Input solicitado
     * @param String $id Id del valor solicitado
     * @return mixer Devuelve el valor del id solicitado. En caso contrario retorna FALSE
     */
    private function get_data($id) {
        $data = Input::get($id);

        // Se valida que el dato ingresado sea válido
        if ($data == NULL) {
            return FALSE;
        }
        return $data;
    }

    /**
     * Valida que el servicio solicitado cumpla con ciertas condiciones
     * @param Service $servicio
     * @return mixer Devuelve TRUE si servicio solicitado cumple con las reglas del negosio, en caso contrario, devuelve el código de error.
     */
    private function valida_servicio(Service $servicio) {
        // Se verifica que el Servicio es válido
        if ($servicio == NULL) {
            return ERROR_SIN_SERVICIO;
        }

        // Se verifica si el Servicio habia sido completado Status =? 6
        if ($servicio->status_id == STATUS_COMPLETADO) {
            return ERROR_ESTATUS_FALLIDO;
        }

        // Se verifica que el servicio no tenga un Driver y que el estatus sea diferente de Sin diver ? Es lo que entendi
        if ($servicio->driver_id != NULL && $servicio->status_id != STATUS_SIN_DRIVER) {
            return ERROR_SIN_DRIVER;
        }
        return TRUE;
    }
    
    /**
     * Actualiza la infomración del Servicio y del Driver
     * @param String $service_id Id del Servicio
     * @param String $driver_id Id del Driver
     * @return Service Servicio actualziado 
     */
    private function actualiza_post_confirm($service_id, $driver_id){
        //Obtenemos el Driver del servicio
        $driver = Driver::find($driver_id);

        // Actualizamos el status y el driver del Servicio
        Driver::update($driver_id, array(
                    "available" => DEFAULT__AVAILABLE
        ));
        
        return Service::update($service_id, array(
                    'driver_id' => $driver_id,
                    'status_id' => STATUS_CON_DRIVER,
                    'car_id' => $driver->car_id
        )); // Devolvemos el Servicio actualizado
    }

    /**
     * Devuelve un Json con el tipo de error indicado
     * @param String $code Codigo del Error
     * @return JSON Estructura del Error
     */
    private function response_error($code) {
        return Response::json(array('error' => $code));
    }

    /**
     * Envía la notificación al usuario
     * @param Service $servicio Servicio al cual se va a enviar la notificación
     * @param String $pushMessage Mensaje de la notificación
     * @return JSON Mensaje de error
     */
    private function enviar_notificacion(Service $servicio, $pushMessage) {
        $push = Push::make();
        // Si no hay un uuid de la aplicación iPhone o Android
        if ($servicio->user->uuid == '') {
            return $this->response_error(ERROR_SIN_ERROR);
        }
        switch ($servicio->user->type) { // En caso de haber más de un tipo de disposivo
            case TYPE_IPHONE: // iPhone
                $pushAns = $push->ios($servicio->user->uuid, $pushMessage, 'honk.wav', 'Open', array('serviceId' => $servicio->id));
                break;

            default: // Android
                $pushAns = $push->android2($servicio->user->uuid, $pushMessage, 'default', 'Open', array('serviceId' => $servicio->id));
                break;
        }
        return $this->response_error(ERROR_SIN_ERROR);
    }

}
