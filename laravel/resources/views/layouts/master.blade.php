<!-- Stored in resources/views/layouts/master.blade.php -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<title>Cube Summation - @yield('title')</title>
	<link rel="stylesheet" href="/css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/jquery-ui/jquery-ui.css" type="text/css" media="all" />
<!--        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>-->
        <script type="text/javascript" src="/jquery-ui/external/jquery/jquery.js"></script>
        <script type="text/javascript" src="/javascript/validaciones.js"></script>
        <script type="text/javascript" src="/javascript/application.js"></script>
        <script type="text/javascript" src="/jquery-ui/jquery-ui.js"></script>
</head>
<body @yield('onready') >

	
	<!-- Header -->
	<div id="header">
		<div class="shell">
			
			<div id="head">
				<h1><a href="#">Cube - Summation</a></h1>
				<?php 
                                /*<div class="right">
					<p>
						Welcome <a href="#"><strong>Administrator</strong></a> | 
						<a href="#">Help</a> |
						<a href="#">Profile Settings</a> |
						<a href="#">Logout</a>
					</p>
				</div>*/
                                ?>
			</div>
			
			<!-- Navigation -->
			<div id="navigation">
				<ul>
				    @yield('menu')
				</ul>
			</div>
			<!-- End Navigation -->
			
		</div>
	</div>
	<!-- End Header -->
	
	
	
	<!-- Content -->
	<div id="content" class="shell"> 
            @yield('content')
	</div>
	
	<!-- End Content -->
</div>

<!-- Footer -->
<div id="footer">
	<p>&copy; Sitename.com. Design by <a href="http://chocotemplates.com">ChocoTemplates.com</a></p>
</div>
<!-- End Footer -->
</body>
</html>