<?php

namespace App\Helpers;

/**
 * Description of matrix
 *
 * @author BetGader
 */
class Matrix {

    private $cube = array();
    private $N = 0;

    function __construct($N, $json = false) {
        $this->N = $N;

        if ($json) {
            $this->setJson($json);
        } else {
            $this->newCube();
        }
    }

    function update($x, $y, $z, $w) {
        $this->cube[$x][$y][$z] = $w;
        return $w;
    }

    function get($x, $y, $z) {
        return $this->cube[$x][$y][$z];
    }

    function query($x1, $y1, $z1, $x2, $y2, $z2) {
        $x = floor (($x1 + $x2) / 2);
        $y = floor (($y1 + $y2) / 2);
        $z = floor (($z1 + $z2) / 2);
        return $this->get($x, $y, $z);
    }

    function setJson($json) {
        $this->cube = json_decode($json);
    }

    function getCube() {
        return json_encode($this->cube);
    }

    function newCube($w = 0) {
        $this->cube = array();
        //Iniciamos la matriz con valores en 0
        for ($x = 1; $x <= $this->N; $x++) {
            $this->cube[$x] = array();
            for ($y = 1; $y <= $this->N; $y++) {
                $this->cube[$x][$y] = array();
                for ($z = 1; $z <= $this->N; $z++) {
                    $this->update($x, $y, $z, $w);
                }
            }
        }
    }

}
