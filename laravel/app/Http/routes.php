<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/Cubic', 'CubicController@index');
Route::post('/Cubic/process', 'CubicController@process');
Route::get('/Cubic/setn/{n}', ['uses' =>'CubicController@setn']);
Route::get('/Cubic/update/{x}/{y}/{z}/{w}', ['uses' =>'CubicController@update']);
Route::get('/Cubic/query/{x1}/{y1}/{z1}/{x2}/{y2}/{z2}', ['uses' =>'CubicController@query']);
