<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Helpers\Matrix;

/**
 * Description of CubicController
 *
 * @author BetGader
 */
class CubicController extends Controller {

    

    public function index() {
        return view('cubic.index');
    }

    public function process(Request $request) {
        $t = $request->input('t');
        
        \Session::put('cubic.t', $t);
        
        return view('cubic.process')->with('t', $t);
    }

    public function setn($n) {
        
        \Session::put('cubic.n', $n);
        
        $matrix = new Matrix($n);
        
        \Session::put('cubic.matrix', $matrix);
        
        return $matrix->getCube();
    }

    public function update($x, $y, $z, $w) {
        $n = \Session::get('cubic.n');
        $matrix = \Session::get('cubic.matrix');
        
        $output = $matrix->update($x, $y, $z, $w);

        \Session::put('cubic.matrix', $matrix);
        
        return $output;
    }
    public function query($x1, $y1, $z1, $x2, $y2, $z2) {
        $n = \Session::get('cubic.n');
        $matrix = \Session::get('cubic.matrix');

        return $matrix->query($x1, $y1, $z1, $x2, $y2, $z2);
    }

}
